const data = require("./1-arrays-jobs.cjs");
const convertSalaryToNumber = require("./problem2.cjs");

function totalSalary(userData){
    const salary = userData.reduce((acc, user) => {
        acc += user.salary;
        return acc;
    }, 0)
    return salary;
}

const sumOfSalary = convertSalaryToNumber(data);
console.log(totalSalary(sumOfSalary));