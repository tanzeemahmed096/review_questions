const data = require("./1-arrays-jobs.cjs");
const convertSalaryToNumber = require("./problem2.cjs");

function avgSalaryBasedOnCountry(userData) {
  const countCountry = userData.reduce((acc, user) => {
    acc[user.location] = acc[user.location] || 0;
    acc[user.location] += 1;
    return acc;
  }, {});

  const sumOfSalary = userData.reduce((acc, user) => {
    acc[user.location] = acc[user.location] || 0;
    acc[user.location] += user.salary;
    return acc;
  }, {});

  const avgSalary = userData.reduce((acc, user) => {
    acc[user.location] = acc[user.location] / countCountry[user.location];
    return acc;
  }, sumOfSalary);

  return avgSalary;
}

const userSalaryInNumber = convertSalaryToNumber(data);
console.log(avgSalaryBasedOnCountry(userSalaryInNumber));
