const data = require("./1-arrays-jobs.cjs");

function convertSalaryToNumber(userData){
    const changeSalary = userData.map(user => {
        let modifiedSalary = user.salary;
        modifiedSalary = parseFloat(modifiedSalary.substring(1, modifiedSalary.length));
        user.salary = modifiedSalary;
        return user;
    })

    return changeSalary;
}

module.exports = convertSalaryToNumber;

const userDataObj = convertSalaryToNumber(data);
console.log(userDataObj);