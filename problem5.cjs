const data = require("./1-arrays-jobs.cjs");
const convertSalaryToNumber = require("./problem2.cjs");

function sumOfSalaryBasedOnLocation(userData) {
  const sumOfSalary = userData.reduce((acc, user) => {
    acc[user.location] = acc[user.location] || 0;
    acc[user.location] += user.salary;
    return acc;
  }, {});

  return sumOfSalary;
}

module.exports = sumOfSalaryBasedOnLocation;
const userSalaryInNumber = convertSalaryToNumber(data);
console.log(sumOfSalaryBasedOnLocation(userSalaryInNumber));
