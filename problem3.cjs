const data = require("./1-arrays-jobs.cjs");
const convertSalaryToNumber = require("./problem2.cjs");

function correctedSalary(userData){
    const modifiedSalary = userData.map(user => {
        const altersalary = user.salary * 10000;
        user.correctSalary = altersalary;
        return user;
    })

    return modifiedSalary;
}

const addSalary = convertSalaryToNumber(data);
console.log(correctedSalary(addSalary));