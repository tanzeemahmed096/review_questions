const data = require("./1-arrays-jobs.cjs");

function webDev(userData){
    const webDevJobs = userData.filter(user => {
        let jobs = user.job;
        jobs = jobs.split(" ");
        return jobs[0] === "Web";
    })
    return webDevJobs;
}

const webDevJobs = webDev(data);
console.log(webDevJobs);